﻿using Harmony;

namespace Media_Controls
{
    [HarmonyPatch(typeof(CockpitRadio))]
    [HarmonyPatch("PlayButton")]
    public class Patch0
    {
        public static bool Prefix()
        {
            MediaControls.PressButton(MediaKey.PlayPause);
            return false;
        }
    }

    [HarmonyPatch(typeof(CockpitRadio))]
    [HarmonyPatch("NextSong")]
    public class Patch1
    {
        public static bool Prefix()
        {
            MediaControls.PressButton(MediaKey.NextTrack);
            return false;
        }
    }

    [HarmonyPatch(typeof(CockpitRadio))]
    [HarmonyPatch("PrevSong")]
    public class Patch2
    {
        public static bool Prefix()
        {
            MediaControls.PressButton(MediaKey.PreviousTrack);
            return false;
        }
    }

    [HarmonyPatch(typeof(VRTwistKnob))]
    [HarmonyPatch("OnLoadVehicleData")]
    public class Patch3
    {
        public static bool Prefix(VRTwistKnob __instance)
        {
            // For the RadioVolume knob, we want to always set it to our default volume when a vehicle is loaded. By default
            // it will use the last saved value, so we set manually set it to ours and then return false.
            if (__instance.gameObject.name == "RadioVolumeKnob")
            {
                var value = MusicVolumeManager.instance.DefaultRadioVolume();
                __instance.startValue = value;
                __instance.SetKnobValue(value);
                return false;
            }

            return true;
        }
    }
}
